package com.epf.tuicodechallenge.threads

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

interface TestThreadScheduler: ThreadScheduler {
    override fun backgroundThread(): Scheduler = Schedulers.trampoline()
    override fun mainThread(): Scheduler = Schedulers.trampoline()
}