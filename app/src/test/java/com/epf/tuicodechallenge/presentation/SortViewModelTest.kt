package com.epf.tuicodechallenge.presentation

import com.epf.domain.search.SearchData
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import android.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.rules.TestRule
import org.junit.Rule



class SortViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    lateinit var sut : SortViewModel

    val list = listOf(
            SearchData(rank = 8, id = 2),
            SearchData(rank = -1, id = 4),
            SearchData(rank = 3, id = 3),
            SearchData(rank = 5, id = 5),
            SearchData(rank = -8, id = 1),
            SearchData(rank = -7, id = 6)
    )

    @Before
    fun setUp() {
        sut = SortViewModel()
    }

    @Test
    fun givenStandardSortThenSortById() {
        sut.updatedList.value = list
        sut.sort(list)

        val expected = listOf(
                SearchData(rank = -7, id = 6),
                SearchData(rank = 5, id = 5),
                SearchData(rank = -1, id = 4),
                SearchData(rank = 3, id = 3),
                SearchData(rank = 8, id = 2),
                SearchData(rank = -8, id = 1)
        )
        assertEquals(expected, sut.updatedList.value)
    }

    @Test
    fun givenRankSortThenSortByRank() {
        sut.updatedList.value = list
        sut.sort(list)
        sut.sort(list)

        val expected = listOf(
                SearchData(rank = 8, id = 2),
                SearchData(rank = 5, id = 5),
                SearchData(rank = 3, id = 3),
                SearchData(rank = -1, id = 4),
                SearchData(rank = -7, id = 6),
                SearchData(rank = -8, id = 1)
        )
        assertEquals(expected, sut.updatedList.value)
    }
}