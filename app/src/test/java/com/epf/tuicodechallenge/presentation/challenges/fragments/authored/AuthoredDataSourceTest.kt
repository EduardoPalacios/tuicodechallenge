package com.epf.tuicodechallenge.presentation.challenges.fragments.authored

import com.epf.data.room.ChallengesDao
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class AuthoredDataSourceTest {

    lateinit var sut: AuthoredDataSource

    @Mock
    lateinit var mockDao: ChallengesDao

    @Before
    fun setUp() {
        sut = AuthoredDataSourceImpl(mockDao)
    }

    @Test
    fun givenMemberIdThenReturnAuthoredChallenges() {
        val memberId = 2
        sut.authoredCache(memberId)
        verify(mockDao).getAllAuthored(memberId)
    }
}