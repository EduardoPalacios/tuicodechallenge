package com.epf.tuicodechallenge.presentation.search

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.epf.domain.search.SearchData
import com.epf.domain.search.UseCase
import com.epf.tuicodechallenge.threads.TestThreadScheduler
import io.reactivex.Maybe
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class UserViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()


    @Mock
    lateinit var mockSearchDataUseCase: UseCase

    private lateinit var sut: UserViewModel

    @Before
    fun setUp() {
        sut = UserViewModel(mockSearchDataUseCase, object : TestThreadScheduler {})
    }

    @Test
    fun givenSearchDataThenBindItToViewField() {
        val name = "eddy"
        val rank = 10
        val bestLanguage = "language"
        val searchData = SearchData(name, rank, bestLanguage)

        `when`(mockSearchDataUseCase.request("eddyVolcano")).thenReturn(Single.just(searchData))

        sut.search("eddyVolcano")

        val expectedCurrentSearchField= "eddy rank: 10 - language score: -1"
        assertEquals(expectedCurrentSearchField, sut.currentSearchData.get())

        sut.updatedCachedLiveData.observeForever(Observer { // FIXME expection thwown but test pass
            val expectedDataToUpdateRecyclerview = SearchData("eddy", 10, "language", -1)
            assertEquals(expectedDataToUpdateRecyclerview, it)
        })
    }

    @Test
    fun givenCachedDataThenBindItToViewField() {
        val name = "eddy"
        val rank = 10
        val bestLanguage = "language"
        val searchData = SearchData(name, rank, bestLanguage)

        val list = listOf(searchData)
        `when`(mockSearchDataUseCase.requestCache()).thenReturn(Maybe.just(list))

        sut.loadCachedData()

        sut.cachedLiveData.observeForever(Observer { // FIXME expection thwown but test pass
           assertEquals(list, it)
        })
    }

}