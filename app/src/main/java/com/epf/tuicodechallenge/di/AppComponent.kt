package com.epf.tuicodechallenge.di

import android.app.Application
import android.content.Context
import com.epf.data.network.NetworkModule
import com.epf.data.room.RoomModule
import com.epf.tuicodechallenge.threads.ThreadScheduler
import com.epf.tuicodechallenge.App
import com.epf.tuicodechallenge.presentation.challenges.ChallengeDetailsActivity
import com.epf.tuicodechallenge.presentation.challenges.ChallengesActivity
import com.epf.tuicodechallenge.presentation.challenges.di.ChallengeDetailsModule
import com.epf.tuicodechallenge.presentation.challenges.di.ChallengesModule
import com.epf.tuicodechallenge.presentation.challenges.fragments.ChallengesFragmentBuilder
import com.epf.tuicodechallenge.presentation.search.SearchActivity
import com.epf.tuicodechallenge.presentation.search.di.SearchModule
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton


@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, ActivityBuilder::class, NetworkModule::class, RoomModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent

    }

    fun inject(app: App)
}

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideThreadScheduler(): ThreadScheduler = object : ThreadScheduler {}

}

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [SearchModule::class])
    abstract fun bindSearchActivity(): SearchActivity

    @ContributesAndroidInjector(modules = [ChallengesModule::class, ChallengesFragmentBuilder::class])
    abstract fun bindChallengesActivity(): ChallengesActivity

    @ContributesAndroidInjector(modules = [ChallengeDetailsModule::class])
    abstract fun bindChallengeDetailsActivity(): ChallengeDetailsActivity

}

