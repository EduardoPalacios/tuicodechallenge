package com.epf.tuicodechallenge.presentation.challenges.fragments.authored

import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.epf.data.room.entities.ChallengesAuthoredEntity
import com.epf.tuicodechallenge.R
import com.epf.tuicodechallenge.extensions.gone
import com.epf.tuicodechallenge.extensions.isNotNullOrEmpty
import com.epf.tuicodechallenge.extensions.launchWithExtras
import com.epf.tuicodechallenge.extensions.visible
import com.epf.tuicodechallenge.presentation.challenges.ChallengeDetailsActivity
import com.epf.tuicodechallenge.presentation.challenges.ChallengesActivity
import com.epf.tuicodechallenge.presentation.search.CHALLENGE_ID
import com.epf.tuicodechallenge.presentation.search.MEMBER_ID
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_challenges_authored.*
import javax.inject.Inject


class ChallengesAuthoredFragment : Fragment() {

    @Inject
    lateinit var viewModel: AuthoredViewModel

    @Inject
    lateinit var authoredAdapter: ChallengesAuthoredAdapter

    private val memberId: Int by lazy { activity?.intent?.getIntExtra(MEMBER_ID, -1) ?: -1 }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_challenges_authored, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        initOnMemberSelectedListener()

        viewModel.authoredCache(memberId).observe(this, Observer {
            it.takeIf { it.isNotNullOrEmpty() }?.let { showList(it) } ?: showEmptyState()
        })

    }

    private fun initRecyclerView() {
        rv_challenges_authored.apply {
            layoutManager = LinearLayoutManager(this@ChallengesAuthoredFragment.activity)
            adapter = authoredAdapter
        }
    }

    private fun showEmptyState() {
        rv_challenges_authored.gone()
        empty_state.visible()
    }

    private fun showList(it: List<ChallengesAuthoredEntity>) {
        empty_state.gone()
        rv_challenges_authored.visible()
        authoredAdapter.add(it)
    }


    private fun initOnMemberSelectedListener() {
        authoredAdapter.selectedMemberStream().subscribe{
            activity?.launchWithExtras<ChallengeDetailsActivity> {  putExtra(CHALLENGE_ID, it) } }
    }
}
