package com.epf.tuicodechallenge.presentation.search.di

import com.epf.data.search.SearchRepositoryImpl
import com.epf.domain.search.SearchData
import com.epf.domain.search.SearchRepository
import com.epf.domain.search.SearchUseCase
import com.epf.domain.search.UseCase
import com.epf.tuicodechallenge.presentation.search.SearchResultAdapter
import dagger.Module
import dagger.Provides

@Module
class SearchModule {

    @Provides
    fun provideSearchResultAdapter(): SearchResultAdapter = SearchResultAdapter()

    @Provides
    fun provideSearchUseCase(useCase: SearchUseCase): UseCase {
        return useCase
    }

    @Provides
    fun provideSearchRepository(repository: SearchRepositoryImpl): SearchRepository<SearchData> {
        return repository
    }

}