package com.epf.tuicodechallenge.presentation.challenges

import android.databinding.ObservableField
import com.epf.domain.challenges.ChallengeDetailsData
import com.epf.domain.challenges.ChallengeDetailsUseCase
import com.epf.domain.challenges.ChallengesUseCase
import com.epf.tuicodechallenge.threads.ThreadScheduler
import com.epf.tuicodechallenge.extensions.scheduleOn
import com.epf.tuicodechallenge.presentation.base.BaseViewModel
import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ChallengesViewModel @Inject constructor(private val challengesUseCase: ChallengesUseCase, private val scheduler: ThreadScheduler) : BaseViewModel() {

    val memberNameField = ObservableField<String>()

    fun bindMember(memberName: String) {
        memberNameField.set(memberName)
    }

    fun requestChallenges(memberName: String, memberId: Int) {
        execute { challengesUseCase.requestCompleted(memberName, memberId) }
        execute { challengesUseCase.requestAuthored(memberName, memberId) }
    }

    private fun execute(request: () -> Completable) {
        addDispossable(challengesDisposable(request))
    }

    private fun challengesDisposable(request: () -> Completable): Disposable {
        return request().scheduleOn(scheduler)
                .subscribe(
                        { /*no op*/ },
                        { println("///// Error loading challenges: $it") })
    }

}

class ChallengeDetailsViewModel @Inject constructor(private val detailUseCase: ChallengeDetailsUseCase, private val scheduler: ThreadScheduler) : BaseViewModel() {

    val nameField = ObservableField<String>()
    val categoryField = ObservableField<String>()
    val createdByField = ObservableField<String>()
    val descriptionField = ObservableField<String>()
    val rankField = ObservableField<String>()
    val attempsField = ObservableField<String>()
    val starsField = ObservableField<String>()
    val voteField = ObservableField<String>()

    fun requestChallengeDetails(challengeId: String) {
        addDispossable(challengesDetailsDispossable(challengeId))
    }

    private fun challengesDetailsDispossable(challengeId: String): Disposable {
        return detailUseCase.requestChallengeDetails(challengeId)
                .scheduleOn(scheduler)
                .subscribe(
                        { bind(it) },
                        { println("///// Error loading challenge details: $it") })
    }

    private fun bind(data: ChallengeDetailsData?) {
        data?.let {
            with(it) {
                nameField.set("Name $name")
                categoryField.set("Category: $category")
                createdByField.set("Created by $createdBy")
                descriptionField.set("Description: $description")
                rankField.set("Rank: $rank")
                attempsField.set("Total attempts: $totalAttempts")
                starsField.set("Stars: $totalStars")
                voteField.set("Score: $voteScore")
            }
        }
    }
}
