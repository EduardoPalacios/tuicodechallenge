package com.epf.tuicodechallenge.presentation.challenges.fragments.completed

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.epf.data.room.entities.CompletedEntity
import com.epf.tuicodechallenge.presentation.base.BaseViewModel
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject

class PagingChallengesViewModel @Inject constructor(private val dataSource: PagingDataSource) : BaseViewModel() {

    private val executor: Executor by lazy { Executors.newFixedThreadPool(5); }

    fun completedCache(memberId: Int): LiveData<PagedList<CompletedEntity>> =
            LivePagedListBuilder(dataSource.getChallengesCompleted(memberId), 20)
                    .setFetchExecutor(executor)
                    .build()

}