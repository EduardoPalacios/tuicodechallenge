package com.epf.tuicodechallenge.presentation.challenges.fragments.completed

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.annotation.NonNull
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.epf.data.room.entities.CompletedEntity
import com.epf.domain.ChallengeRequest
import com.epf.domain.search.SearchData
import com.epf.tuicodechallenge.R
import com.epf.tuicodechallenge.databinding.ChallengesCompletedRowBinding
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject


class ChallengesCompletedAdapter : PagedListAdapter<CompletedEntity, CompletedViewHolder>(DIFF_CALLBACK) {

    private val onMemberSelected = PublishSubject.create<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompletedViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ChallengesCompletedRowBinding = DataBindingUtil.inflate(layoutInflater, R.layout.challenges_completed_row, parent, false)
        return CompletedViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CompletedViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.completedEntity = item
        holder.binding.root.setOnClickListener { onMemberSelected.onNext(item?.completed_id.orEmpty()) }
    }

    fun selectedMemberStream(): Flowable<String> = onMemberSelected.toFlowable(BackpressureStrategy.DROP)
}


class CompletedViewHolder(val binding: ChallengesCompletedRowBinding) : RecyclerView.ViewHolder(binding.root) {

}

var DIFF_CALLBACK: DiffUtil.ItemCallback<CompletedEntity> = object : DiffUtil.ItemCallback<CompletedEntity>() {
    override fun areItemsTheSame(@NonNull oldItem: CompletedEntity, @NonNull newItem: CompletedEntity): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(@NonNull oldItem: CompletedEntity, @NonNull newItem: CompletedEntity): Boolean {
        return oldItem == newItem
    }
}