package com.epf.tuicodechallenge.presentation.search

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.epf.tuicodechallenge.R
import com.epf.tuicodechallenge.databinding.ActivitySearchBinding
import com.epf.tuicodechallenge.extensions.launchWithExtras
import com.epf.tuicodechallenge.presentation.SortViewModel
import com.epf.tuicodechallenge.presentation.base.BaseActivity
import com.epf.tuicodechallenge.presentation.challenges.ChallengesActivity
import com.jakewharton.rxbinding2.widget.RxTextView
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_search.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: UserViewModel

    @Inject
    lateinit var sortViewModel: SortViewModel

    @Inject
    lateinit var searchResultAdapter: SearchResultAdapter

    private lateinit var binder: ActivitySearchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
        initDataBinding()
        initSearchInput()
        initSortListener()
        initOnMemberSelectedListener()
        viewModel.cachedLiveData.observe(this, Observer { it?.let { searchResultAdapter.add(it) } })
        viewModel.updatedCachedLiveData.observe(this, Observer { it?.let { searchResultAdapter.add(it) } })
        sortViewModel.updatedList.observe(this, Observer { it?.let { searchResultAdapter.replace(it) } })
        viewModel.loadCachedData()

    }

    private fun initOnMemberSelectedListener() {
        searchResultAdapter.selectedMemberStream().subscribe{
            launchWithExtras<ChallengesActivity> { putExtra(MEMBER_NAME,it.name); putExtra(MEMBER_ID,it.id) } }
    }

    private fun initSortListener() {
        btn_sort_items.setOnClickListener { sortViewModel.sort(searchResultAdapter.list) }
    }

    private fun initSearchInput() {
        RxTextView.textChanges(et_search_input)
                .debounce(300, TimeUnit.MILLISECONDS)
                .subscribe { viewModel.search(it.toString()) }
    }


    private fun initDataBinding() {
        binder = DataBindingUtil.setContentView(this, R.layout.activity_search)
        binder.viewModel = viewModel
        binder.rvPreviousSearch.apply {
            layoutManager = LinearLayoutManager(this@SearchActivity)
            adapter = searchResultAdapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_search, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}


const val MEMBER_NAME = "MEMBER_NAME_EXTRA"
const val MEMBER_ID = "MEMBER_ID_EXTRA"
const val CHALLENGE_ID = "CHALLENGE_ID_EXTRA"