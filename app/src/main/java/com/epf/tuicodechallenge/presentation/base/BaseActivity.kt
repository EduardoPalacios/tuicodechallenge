package com.epf.tuicodechallenge.presentation.base

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.epf.tuicodechallenge.presentation.error.ErrorViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var errorViewModel: ErrorViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        errorViewModel.noConnectivity.observe(this, Observer { it?.let { showToast(it) } })
        errorViewModel.notFound.observe(this, Observer { Log.i("BaseActivity", "Error: $it") })
        errorViewModel.unknownError.observe(this, Observer { it?.let { showToast(it) } })
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}