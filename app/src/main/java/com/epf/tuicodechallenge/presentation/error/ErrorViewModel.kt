package com.epf.tuicodechallenge.presentation.error

import android.arch.lifecycle.MutableLiveData
import com.epf.domain.error.ErrorSource
import com.epf.domain.error.ErrorType
import com.epf.tuicodechallenge.threads.ThreadScheduler
import com.epf.tuicodechallenge.extensions.scheduleOn
import com.epf.tuicodechallenge.presentation.base.BaseViewModel
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ErrorViewModel @Inject constructor(errorSource: ErrorSource, private val scheduler: ThreadScheduler) : BaseViewModel() {

    val noConnectivity = MutableLiveData<String>()
    val notFound = MutableLiveData<String>()
    val unknownError = MutableLiveData<String>()

    init {
        addDispossable(errorDisposable(errorSource))

    }

    private fun errorDisposable(errorSource: ErrorSource): Disposable {
        return errorSource.onErrorFlowable()
                .scheduleOn(scheduler)
                .doOnNext {
                    when (it) {
                        ErrorType.NotFound -> notFound.value = "Not found"
                        ErrorType.NoConnectivity -> noConnectivity.value = "No connectivity"
                        ErrorType.Unknown -> unknownError.value = "Unknown error!"
                    }
                }
                .subscribe()
    }
}