package com.epf.tuicodechallenge.presentation.challenges.fragments.completed

import android.arch.paging.DataSource
import com.epf.data.network.ApiService
import com.epf.data.room.ChallengesDao
import com.epf.data.room.entities.CompletedEntity
import javax.inject.Inject

interface PagingDataSource {
    fun getChallengesCompleted(memberId: Int): DataSource.Factory<Int, CompletedEntity>
}

class PagingDataSourceImpl @Inject constructor(private val dao: ChallengesDao,private val apiService: ApiService) :
        PagingDataSource {

    override fun getChallengesCompleted(memberId: Int): DataSource.Factory<Int, CompletedEntity> {
        return dao.getChallengesCompleted(memberId)
    }
}