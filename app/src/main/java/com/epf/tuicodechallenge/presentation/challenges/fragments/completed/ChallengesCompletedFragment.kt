package com.epf.tuicodechallenge.presentation.challenges.fragments.completed

import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.epf.tuicodechallenge.R
import com.epf.tuicodechallenge.extensions.launchWithExtras
import com.epf.tuicodechallenge.presentation.challenges.ChallengeDetailsActivity
import com.epf.tuicodechallenge.presentation.challenges.ChallengesViewModel
import com.epf.tuicodechallenge.presentation.search.CHALLENGE_ID
import com.epf.tuicodechallenge.presentation.search.MEMBER_ID
import com.epf.tuicodechallenge.presentation.search.MEMBER_NAME
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_challenges_completed.*
import javax.inject.Inject

class ChallengesCompletedFragment : Fragment() {

    @Inject
    lateinit var paginViewModel: PagingChallengesViewModel

    @Inject
    lateinit var viewModel: ChallengesViewModel

    @Inject
    lateinit var challengesAdapter: ChallengesCompletedAdapter

    private val memberName: String by lazy { activity?.intent?.getStringExtra(MEMBER_NAME).orEmpty() }

    private val memberId: Int by lazy { activity?.intent?.getIntExtra(MEMBER_ID, -1) ?: -1 }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_challenges_completed, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        initOnMemberSelectedListener()
        paginViewModel.completedCache(memberId).observe(this, Observer {
            challengesAdapter.submitList(it)
        })
    }

    private fun initRecyclerView() {
        rv_challenges_completed.apply {
            layoutManager = LinearLayoutManager(this@ChallengesCompletedFragment.activity)
            adapter = challengesAdapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    requestNewPage()
                }

                private fun requestNewPage() {
                    val lastVisibleItemPosition = (layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
                    val lastPosition = adapter.itemCount - 1
                    if (adapter.itemCount > 0 && lastVisibleItemPosition >= lastPosition) {
                        viewModel.requestChallenges(memberName, memberId)
                    }
                }
            })
        }
    }

    private fun initOnMemberSelectedListener() {
        challengesAdapter.selectedMemberStream().subscribe {
            activity?.launchWithExtras<ChallengeDetailsActivity> { putExtra(CHALLENGE_ID, it) }
        }
    }
}