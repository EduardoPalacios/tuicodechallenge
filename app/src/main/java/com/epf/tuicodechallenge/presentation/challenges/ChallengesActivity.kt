package com.epf.tuicodechallenge.presentation.challenges

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import com.epf.tuicodechallenge.R
import com.epf.tuicodechallenge.databinding.ActivityChallengesBinding
import com.epf.tuicodechallenge.presentation.base.BaseActivity
import com.epf.tuicodechallenge.presentation.challenges.fragments.authored.ChallengesAuthoredFragment
import com.epf.tuicodechallenge.presentation.challenges.fragments.completed.ChallengesCompletedFragment
import com.epf.tuicodechallenge.presentation.search.MEMBER_ID
import com.epf.tuicodechallenge.presentation.search.MEMBER_NAME
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_challenges.*
import javax.inject.Inject

class ChallengesActivity : BaseActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatcher: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var viewModel: ChallengesViewModel

    private lateinit var binding: ActivityChallengesBinding

    private val memberName: String by lazy { intent?.getStringExtra(MEMBER_NAME).orEmpty() }

    private val memberId: Int by lazy { intent?.getIntExtra(MEMBER_ID, -1) ?: -1 }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatcher


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDataBinding()
        initNavBottomListener()
        viewModel.bindMember(memberName)
        viewModel.requestChallenges(memberName, memberId)
        showFragment(ChallengesCompletedFragment())
    }

    private fun initNavBottomListener() {
        bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_completed -> {
                    showFragment(ChallengesCompletedFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.action_authored -> {
                    showFragment(ChallengesAuthoredFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                else -> {
                    true
                }
            }
        }
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit()
    }

    private fun initDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_challenges)
        binding.viewModel = viewModel
    }
}