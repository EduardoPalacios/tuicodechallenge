package com.epf.tuicodechallenge.presentation.challenges.fragments.authored

import android.arch.lifecycle.LiveData
import com.epf.data.room.ChallengesDao
import com.epf.data.room.entities.ChallengesAuthoredEntity
import javax.inject.Inject

interface AuthoredDataSource {
    fun authoredCache(memberId: Int): LiveData<List<ChallengesAuthoredEntity>>
}

class AuthoredDataSourceImpl @Inject constructor(private val dao: ChallengesDao): AuthoredDataSource{

    override fun authoredCache(memberId: Int): LiveData<List<ChallengesAuthoredEntity>> {
        return dao.getAllAuthored(memberId)
    }
}