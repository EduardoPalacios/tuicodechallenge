package com.epf.tuicodechallenge.presentation.search

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.epf.domain.search.SearchData
import com.epf.tuicodechallenge.R
import com.epf.tuicodechallenge.databinding.SearchResultRowBinding
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject

class SearchResultAdapter : RecyclerView.Adapter<SearchResultViewHolder>() {

    companion object {
        const val LIMIT = 5
    }
    val list = ArrayList<SearchData>(LIMIT)

    private val onMemberSelected = PublishSubject.create<SearchData>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binder: SearchResultRowBinding = DataBindingUtil.inflate(layoutInflater, R.layout.search_result_row, parent, false)
        return SearchResultViewHolder(binder)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        val data = list[position]
        holder.binding.searchData = data
        holder.binding.root.setOnClickListener { onMemberSelected.onNext(data) }
    }

    fun add(newData: SearchData) {
        if (itemCount == LIMIT) {
            list.removeAt(itemCount -1)
        }
        list.add(0,newData)
        notifyDataSetChanged()
    }

    fun add(newData: List<SearchData>) {
        list.addAll(newData)
        notifyDataSetChanged()
    }

    fun replace(newData: List<SearchData>) {
        list.clear()
        add(newData)
    }

    fun selectedMemberStream(): Flowable<SearchData> = onMemberSelected.toFlowable(BackpressureStrategy.DROP)

}


class SearchResultViewHolder(val binding: SearchResultRowBinding) : RecyclerView.ViewHolder(binding.root) {
}
