package com.epf.tuicodechallenge.presentation.search

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import android.util.Log
import com.epf.domain.search.SearchData
import com.epf.domain.search.UseCase
import com.epf.domain.search.isValid
import com.epf.tuicodechallenge.threads.ThreadScheduler
import com.epf.tuicodechallenge.extensions.scheduleOn
import com.epf.tuicodechallenge.presentation.base.BaseViewModel
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class UserViewModel @Inject constructor(
        private val searchUseCase: UseCase,
        private val scheduler: ThreadScheduler) : BaseViewModel() {


    val cachedLiveData = MutableLiveData<List<SearchData>>()
    val updatedCachedLiveData = MutableLiveData<SearchData>()
    val currentSearchData = ObservableField<String>()

    fun loadCachedData() {
        compositeDisposable.add(disposableCachedData())
    }

    private fun disposableCachedData(): Disposable {
        return searchUseCase.requestCache()
                .scheduleOn(scheduler)
                .subscribe(
                        { cachedLiveData.value = it },
                        { Log.e("UserViewModel", "error requesting cached data: $it") }
                )
    }

    fun search(idOrName: String) {
        compositeDisposable.add(disposableSearchRequest(idOrName))
    }

    private fun disposableSearchRequest(idOrName: String): Disposable {
        return searchUseCase.request(idOrName)
                .scheduleOn(scheduler)
                .subscribe(
                        { bindSearchResult(it) },
                        { Log.e("UserViewModel", "error requesting user: $it") }
                )
    }

    private fun bindSearchResult(it: SearchData) {
        if (it.isValid()) {
            bindCurrentSearch(it); updatedCachedData(it)
        }
    }

    private fun bindCurrentSearch(data: SearchData) {
        with(data) {
            currentSearchData.set("$name rank: $rank - $bestLanguage score: $score")
        }

    }

    private fun updatedCachedData(data: SearchData) {
        updatedCachedLiveData.value = data
    }

}