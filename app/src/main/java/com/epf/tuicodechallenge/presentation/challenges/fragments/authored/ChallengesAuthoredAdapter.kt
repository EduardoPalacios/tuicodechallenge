package com.epf.tuicodechallenge.presentation.challenges.fragments.authored

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.epf.data.room.entities.ChallengesAuthoredEntity
import com.epf.tuicodechallenge.R
import com.epf.tuicodechallenge.databinding.ChallengesAuthoredRowBinding
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject

class ChallengesAuthoredAdapter : RecyclerView.Adapter<AuthoredViewHolder>() {

    private val onMemberSelected = PublishSubject.create<String>()

    private val list = ArrayList<ChallengesAuthoredEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AuthoredViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ChallengesAuthoredRowBinding = DataBindingUtil.inflate(inflater, R.layout.challenges_authored_row, parent, false)
        return AuthoredViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AuthoredViewHolder, position: Int) {
        val item = list[position]
        holder.binding.authoredEntity = item
        holder.binding.root.setOnClickListener { onMemberSelected.onNext(item.authoredId) }
    }

    override fun getItemCount(): Int = list.count()

    fun add(authoredEntities: List<ChallengesAuthoredEntity>) {
        list.addAll(authoredEntities)
        notifyDataSetChanged()
    }

    fun selectedMemberStream(): Flowable<String> = onMemberSelected.toFlowable(BackpressureStrategy.DROP)
}


class AuthoredViewHolder(val binding: ChallengesAuthoredRowBinding) : RecyclerView.ViewHolder(binding.root)