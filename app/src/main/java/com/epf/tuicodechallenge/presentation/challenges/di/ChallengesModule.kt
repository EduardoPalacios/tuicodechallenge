package com.epf.tuicodechallenge.presentation.challenges.di

import com.epf.data.challenges.ChallengeDetailsRepositoryImpl
import com.epf.data.challenges.ChallengesRepositoryImpl
import com.epf.domain.challenges.*
import com.epf.tuicodechallenge.presentation.challenges.fragments.completed.PagingDataSource
import com.epf.tuicodechallenge.presentation.challenges.fragments.completed.PagingDataSourceImpl
import dagger.Module
import dagger.Provides

@Module
class ChallengesModule {

    @Provides
    fun provideChallengesUseCase(useCase: ChallengesUseCaseImpl): ChallengesUseCase {
        return useCase
    }

    @Provides
    fun provideChallengesRepository(repository: ChallengesRepositoryImpl): ChallengesRepository {
        return repository
    }

    @Provides
    fun providePagingDataSource(source: PagingDataSourceImpl): PagingDataSource {
        return source
    }

}

@Module
class ChallengeDetailsModule {

    @Provides
    fun provideChallengesDetailUseCase(useCase: ChallengeDetailsUseCaseImpl): ChallengeDetailsUseCase {
        return useCase
    }

    @Provides
    fun provideChallengeDetailsRepository(repository: ChallengeDetailsRepositoryImpl): ChallengeDetailsRepository {
        return repository
    }

}