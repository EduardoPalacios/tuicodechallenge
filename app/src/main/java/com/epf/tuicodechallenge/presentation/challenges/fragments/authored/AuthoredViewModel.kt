package com.epf.tuicodechallenge.presentation.challenges.fragments.authored

import android.arch.lifecycle.LiveData
import com.epf.data.room.entities.ChallengesAuthoredEntity
import javax.inject.Inject

class AuthoredViewModel @Inject constructor(private val dataSource: AuthoredDataSource) {

    fun authoredCache(memberId: Int): LiveData<List<ChallengesAuthoredEntity>> = dataSource.authoredCache(memberId)
}