package com.epf.tuicodechallenge.presentation

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.epf.domain.search.SearchData
import com.epf.tuicodechallenge.presentation.base.BaseViewModel
import javax.inject.Inject

class SortViewModel @Inject constructor(): BaseViewModel() {

    val  updatedList = MutableLiveData<List<SearchData>>()
    private var type: SortType = SortType.Standard

    fun sort(list: List<SearchData>) {
        if (type == SortType.Standard) {
            type = SortType.Rank
            updatedList.value = list.sortedByDescending { it.id }
        } else {
            type = SortType.Standard
            updatedList.value = list.sortedByDescending { it.rank }
        }

    }

}

sealed class SortType {
    object Standard : SortType()
    object Rank : SortType()

}