package com.epf.tuicodechallenge.presentation.challenges

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.epf.tuicodechallenge.R
import com.epf.tuicodechallenge.databinding.ActivityChallengeDetailsBinding
import com.epf.tuicodechallenge.presentation.base.BaseActivity
import com.epf.tuicodechallenge.presentation.search.CHALLENGE_ID
import javax.inject.Inject

class ChallengeDetailsActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: ChallengeDetailsViewModel

    private lateinit var binding: ActivityChallengeDetailsBinding

    private val challengeId: String by lazy { intent?.getStringExtra(CHALLENGE_ID).orEmpty() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDataBinding()
        viewModel.requestChallengeDetails(challengeId)
    }

    private fun initDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_challenge_details)
        binding.viewModel = viewModel
    }
}