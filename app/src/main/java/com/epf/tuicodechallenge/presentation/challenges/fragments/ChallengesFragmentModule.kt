package com.epf.tuicodechallenge.presentation.challenges.fragments

import com.epf.tuicodechallenge.presentation.challenges.fragments.authored.AuthoredDataSource
import com.epf.tuicodechallenge.presentation.challenges.fragments.authored.AuthoredDataSourceImpl
import com.epf.tuicodechallenge.presentation.challenges.fragments.authored.ChallengesAuthoredAdapter
import com.epf.tuicodechallenge.presentation.challenges.fragments.authored.ChallengesAuthoredFragment
import com.epf.tuicodechallenge.presentation.challenges.fragments.completed.ChallengesCompletedAdapter
import com.epf.tuicodechallenge.presentation.challenges.fragments.completed.ChallengesCompletedFragment
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector


@Module
class ChallengesCompletedFragmentModule {

    @Provides
    fun provideChallengesCompletedFragment(fragment: ChallengesCompletedFragment) = fragment

    @Provides
    fun provideChallengesCompletedAdapter(): ChallengesCompletedAdapter = ChallengesCompletedAdapter()
}

@Module
class ChallengesAuthoredFragmentModule {

    @Provides
    fun provideChallengesCompletedFragment(fragment: ChallengesAuthoredFragment) = fragment

    @Provides
    fun provideAuthoredDataSource(dataSource: AuthoredDataSourceImpl): AuthoredDataSource = dataSource

    @Provides
    fun provideAuthoredAdapter(): ChallengesAuthoredAdapter = ChallengesAuthoredAdapter()

}

@Module
abstract class ChallengesFragmentBuilder {

    @ContributesAndroidInjector(modules = [ChallengesCompletedFragmentModule::class])
    abstract fun bindCompletedFragmen(): ChallengesCompletedFragment

    @ContributesAndroidInjector(modules = [ChallengesAuthoredFragmentModule::class])
    abstract fun bindAuthoredFragment(): ChallengesAuthoredFragment
}