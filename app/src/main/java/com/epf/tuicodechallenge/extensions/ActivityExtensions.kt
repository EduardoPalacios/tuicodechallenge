package com.epf.tuicodechallenge.extensions

import android.app.Activity
import android.content.Intent


inline fun <reified T : Any> Activity.launch() {
    startActivity(Intent(this, T::class.java))
}

inline fun <reified T : Any> Activity.launchWithExtras(putExtras: Intent.() -> Unit) {
    startActivity(Intent(this, T::class.java).apply { putExtras() })
}

inline fun <reified T : Any> Activity.launchWithExtrasForResult(requestCode: Int, putExtras: Intent.() -> Unit) {
    val intent = Intent(this, T::class.java).apply { putExtras() }
    startActivityForResult(intent, requestCode)
}


inline fun <reified T : Any> Activity.launchForResult(requestCode: Int) {
    startActivityForResult(Intent(this, T::class.java), requestCode)
}