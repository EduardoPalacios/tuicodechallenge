package com.epf.tuicodechallenge.extensions

import com.epf.tuicodechallenge.threads.ThreadScheduler
import io.reactivex.*


fun <T> Observable<T>.scheduleOn(scheduler: ThreadScheduler): Observable<T> {
    return this.subscribeOn(scheduler.backgroundThread())
            .observeOn(scheduler.mainThread())
}

fun <T> Single<T>.scheduleOn(scheduler: ThreadScheduler): Single<T> {
    return this.subscribeOn(scheduler.backgroundThread())
            .observeOn(scheduler.mainThread())
}

fun <T> Flowable<T>.scheduleOn(scheduler: ThreadScheduler): Flowable<T> {
    return this.subscribeOn(scheduler.backgroundThread())
            .observeOn(scheduler.mainThread())
}

fun <T> Maybe<T>.scheduleOn(scheduler: ThreadScheduler): Maybe<T> {
    return this.subscribeOn(scheduler.backgroundThread())
            .observeOn(scheduler.mainThread())
}

fun Completable.scheduleOn(scheduler: ThreadScheduler): Completable {
    return this.subscribeOn(scheduler.backgroundThread())
            .observeOn(scheduler.mainThread())
}