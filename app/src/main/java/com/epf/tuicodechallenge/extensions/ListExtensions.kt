package com.epf.tuicodechallenge.extensions


fun <T> List<T>?.isNotNullOrEmpty(): Boolean = this != null && this.isNotEmpty()