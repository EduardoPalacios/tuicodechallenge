package com.epf.tuicodechallenge.threads

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface ThreadScheduler {
    fun backgroundThread(): Scheduler = Schedulers.io()
    fun mainThread(): Scheduler = AndroidSchedulers.mainThread()
}