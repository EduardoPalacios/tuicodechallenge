package com.epf.tuicodechallenge.presentation.search

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.FailureHandler
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import com.epf.tuicodechallenge.R
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.atomic.AtomicBoolean


@RunWith(AndroidJUnit4::class)
class SearchActivityTest {

    @get:Rule
    var mActivityRule: ActivityTestRule<SearchActivity> = ActivityTestRule(SearchActivity::class.java)

    @Test
    fun enterNameInSearchEditText() {
        enterName()
    }

    @Test
    fun enterNameInSearchEditTextAndWaitForResult() {
        enterName()
        waitToAddUser()
    }

    @Test
    fun addManyUsers() { // this action can fail due to the network if takes too long
        addUsersAndWait()
    }

    private fun addUsersAndWait() {
        enterName()
        waitToAddUser()
        enterName("marc")
        waitToAddUser("marc")
        enterName("g964")
        waitToAddUser("g964")
        enterName("peter")
        waitToAddUser("peter")
        enterName("smile67")
        waitToAddUser("smile67")
    }

    @Test
    fun sortResultsAfterClickFabButton() {
        sort()
    }

    private fun sort() {
        onView(withId(R.id.btn_sort_items)).perform(click())
    }


    @Test
    fun selectUser() {
        enterName()
        waitToAddUser()
        clickOnItem()
        waitUntilCompleteChallengesAreLoaded() // this step can fail due to the network
        onView(allOf(withId(R.id.action_authored), withText("Authored"))).perform(click())

    }

    @Test
    fun openChallengesScreen() {
        enterName()
        waitToAddUser()
        clickOnItem()
        waitUntilCompleteChallengesAreLoaded() // this step can fail due to the network
    }

    private fun waitUntilCompleteChallengesAreLoaded() {
        waitFor(allOf(withId(R.id.name), ViewMatchers.withText(containsString("Vowel Count"))))
                .check(matches(isDisplayed()))
    }

    private fun clickOnItem() {
        onView(allOf(withId(R.id.name), ViewMatchers.withText(containsString("eddyVolcano")))).perform(click())
    }

    private fun waitToAddUser(name: String = "eddyVolcano") {
        waitFor(allOf(withId(R.id.name), ViewMatchers.withText(containsString(name))))
                .check(matches(isDisplayed()))
    }

    private fun enterName(name: String = "eddyVolcano") {
        onView(withId(R.id.et_search_input)).check(matches((isDisplayed()))).perform(clearText(), typeText(name))
    }

}

fun waitFor(viewMatcher: Matcher<View>): ViewInteraction {
    val timeToWait = now() + 30000

    try {
        val assertionPassed = AtomicBoolean(false)
        val nullFailureHandler = FailureHandler { _, _ -> assertionPassed.set(false) }

        do {
            checkViewIsDisplayed(assertionPassed, viewMatcher, nullFailureHandler)
            if (assertionPassed.get()) {
                break
            }

            sleep(100)
        } while (now() < timeToWait)

    } catch (e: InterruptedException) {
        e.printStackTrace()
    }
    return onView(viewMatcher).check(matches(isDisplayed()))
}

private fun checkViewIsDisplayed(assertionPassed: AtomicBoolean, viewMatcher: Matcher<View>, nullFailureHandler: FailureHandler) {
    assertionPassed.set(true)
    onView(viewMatcher)
            .withFailureHandler(nullFailureHandler)
            .check(matches(isDisplayed()))
}

private fun sleep(time: Long) {
    Thread.sleep(time)
}

private fun now() = System.currentTimeMillis()