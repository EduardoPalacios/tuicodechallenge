package com.epf.data.search

import com.epf.data.network.ApiService
import com.epf.data.network.models.search.User
import com.epf.data.network.models.search.toSearchData
import com.epf.data.network.models.search.toUserEntity
import com.epf.data.room.UserDao
import com.epf.data.room.entities.toSearchData
import com.epf.domain.error.ErrorStream
import com.epf.domain.search.SearchData
import com.epf.domain.search.SearchRepository
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject


class SearchRepositoryImpl @Inject constructor(private val apiService: ApiService, private val dao: UserDao, private val errorStream: ErrorStream) : SearchRepository<SearchData> {

    override fun getUser(idOrName: String): Single<SearchData> {
        // if user already exist dont search
        if (userExist(idOrName)) return Single.just(SearchData())
        return apiService.getUser(idOrName)
                .doOnError { errorStream.processException(it) }
                .map {
                    if (it.isSuccessful) {
                        val user = it.body()
                        val id = saveUser(user)
                        user?.toSearchData(id)
                    } else {
                        errorStream.processErrorCode(it.code())
                        SearchData()
                    }

                }


    }

    private fun userExist(idOrName: String): Boolean {
        val userBy = dao.findUserBy(idOrName)
        return userBy == idOrName
    }

    private fun saveUser(it: User?): Int {
        return it?.let {
            dao.addMember(it.toUserEntity())
            return@let dao.getLastUser().id
        } ?: -1
    }

    override fun getCachedData(): Maybe<List<SearchData>> {
        return dao.getRecentUsers().map { it.map { it.toSearchData() } }
    }
}
