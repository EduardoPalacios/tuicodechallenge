package com.epf.data.challenges

import com.epf.data.network.ApiService
import com.epf.data.network.models.challenges.details.toChallengeDetailsData
import com.epf.domain.challenges.ChallengeDetailsData
import com.epf.domain.challenges.ChallengeDetailsRepository
import com.epf.domain.error.ErrorStream
import io.reactivex.Single
import javax.inject.Inject

class ChallengeDetailsRepositoryImpl @Inject constructor(private val apiService: ApiService, private val errorStream: ErrorStream) : ChallengeDetailsRepository {

    override fun requestChallengeDetails(challengeId: String): Single<ChallengeDetailsData> {
        return apiService.getChallengeDetails(challengeId)
                .doOnError { errorStream.processException(it) }
                .map {
                    if (it.isSuccessful) {
                        it.body()?.toChallengeDetailsData()
                    } else {
                        errorStream.processErrorCode(it.code())
                        ChallengeDetailsData()
                    }
                }

    }
}