package com.epf.data.challenges

import com.epf.data.network.ApiService
import com.epf.data.network.models.challenges.ChallengesAuthored
import com.epf.data.network.models.challenges.ChallengesCompleted
import com.epf.data.network.models.challenges.toAuthoredEntity
import com.epf.data.network.models.challenges.toEntity
import com.epf.data.room.ChallengesDao
import com.epf.data.room.PageDao
import com.epf.data.room.entities.PageEntity
import com.epf.domain.challenges.ChallengesRepository
import com.epf.domain.error.ErrorStream
import io.reactivex.Completable
import javax.inject.Inject

class ChallengesRepositoryImpl @Inject constructor(
        private val apiService: ApiService,
        private val dao: ChallengesDao,
        private val pageDao: PageDao,
        private val errorStream: ErrorStream) : ChallengesRepository {

    override fun getChallengesCompleted(idOrName: String, memberId: Int): Completable {

        val entity = pageDao.getPage(memberId)
        val nextPage = getNextPage(entity)
        println("//// next page is $nextPage")
        if (nextPage == -1) {
            return Completable.complete()
        }

        println("//// last page for $idOrName is $nextPage")
        return apiService.getChallengesCompleted(idOrName, nextPage)
                .doOnSuccess {
                    if (it.isSuccessful) {
                        cacheData(it.body(), memberId, idOrName, nextPage)
                    } else {
                        errorStream.processErrorCode(it.code())
                    }
                }
                .doOnError { errorStream.processException(it) }
                .toCompletable()
    }

    private fun cacheData(it: ChallengesCompleted?, memberId: Int, idOrName: String, nextPage: Int) {
        it?.let {
            dao.addChallengesCompleted(it.toEntity(memberId))
            it.data?.forEach { dao.addCompleted(it.toEntity(memberId)) }
            pageDao.add(PageEntity(memberId, idOrName, nextPage, it.totalPages ?: 0))
        }
    }

    private fun getNextPage(entity: PageEntity?): Int {
        return entity?.let {
            when {
                entity.page == -1 -> 0
                entity.page == entity.total -> -1
                else -> entity.page.plus(1)
            }
        } ?: 0
    }

    override fun getChallengesAuthored(idOrName: String, memberId: Int): Completable {
        return apiService.getChallengesAuthored(idOrName)
                .doOnSuccess {
                    if (it.isSuccessful) {
                        it.body()?.let {
                            dao.addAuthored(mapToEntity(it, memberId))
                        }
                    } else {
                        errorStream.processErrorCode(it.code())
                    }

                }
                .doOnError { errorStream.processException(it) }
                .toCompletable()
    }

    private fun mapToEntity(it: ChallengesAuthored, memberId: Int) =
            it.challenges?.map { it.toAuthoredEntity(memberId) } ?: emptyList()


}