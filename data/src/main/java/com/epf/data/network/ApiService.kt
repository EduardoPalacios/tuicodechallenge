package com.epf.data.network

import com.epf.data.network.models.challenges.ChallengesAuthored
import com.epf.data.network.models.challenges.ChallengesCompleted
import com.epf.data.network.models.challenges.details.ChallengeDetails
import com.epf.data.network.models.search.User
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    companion object {
        const val USER = "api/v1/users/{idOrUserName}"
        const val CHALLENGES_COMPLETED = "api/v1/users/{idOrUserName}/code-challenges/completed"
        const val CHALLENGES_AUTHORED = "api/v1/users/{idOrUserName}/code-challenges/authored"
        const val CHALLENGES_DETAILS = "api/v1/code-challenges/{challengeId}"
    }

    @GET(USER)
    fun getUser(@Path("idOrUserName") id: String): Single<Response<User>>

    @GET(CHALLENGES_COMPLETED)
    fun getChallengesCompleted(@Path("idOrUserName") id: String, @Query("page") page: Int = 0): Single<Response<ChallengesCompleted>>

    @GET(CHALLENGES_AUTHORED)
    fun getChallengesAuthored(@Path("idOrUserName") id: String): Single<Response<ChallengesAuthored>>

    @GET(CHALLENGES_DETAILS)
    fun getChallengeDetails(@Path("challengeId") id: String): Single<Response<ChallengeDetails>>


}