package com.epf.data.network.models.challenges.details

import com.google.gson.annotations.SerializedName

class Unresolved(
        @SerializedName("issues")
        var issues: Int? = null,
        @SerializedName("suggestions")
        var suggestions: Int? = null

)
