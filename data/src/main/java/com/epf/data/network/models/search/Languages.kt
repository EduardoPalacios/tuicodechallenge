package com.epf.data.network.models.search

import android.arch.persistence.room.TypeConverters
import com.epf.data.room.converters.LanguagesConverter
import com.google.gson.*
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import java.lang.reflect.Type

@JsonAdapter(LanguagesDeserializer::class)
@TypeConverters(LanguagesConverter::class)
data class Languages constructor(

        @SerializedName("language")
        val languages: List<Pair<String, Language>>
)


class LanguagesDeserializer : JsonDeserializer<Languages> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Languages {
        val languages = (json as JsonObject).entrySet().map {
            val language = Gson().fromJson(it.value, Language::class.java)
            it.key to language
        }
        return Languages(languages)
    }
}

