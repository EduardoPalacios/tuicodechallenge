package com.epf.data.network.models.challenges.details

import com.epf.domain.challenges.ChallengeDetailsData
import com.google.gson.annotations.SerializedName

class ChallengeDetails(
        @SerializedName("id")
        var id: String? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("slug")
        var slug: String? = null,
        @SerializedName("category")
        var category: String? = null,
        @SerializedName("publishedAt")
        var publishedAt: String? = null,
        @SerializedName("approvedAt")
        var approvedAt: String? = null,
        @SerializedName("languages")
        var languages: List<String>? = null,
        @SerializedName("url")
        var url: String? = null,
        @SerializedName("rank")
        var rank: Rank? = null,
        @SerializedName("createdAt")
        var createdAt: String? = null,
        @SerializedName("createdBy")
        var createdBy: CreatedBy? = null,
        @SerializedName("approvedBy")
        var approvedBy: ApprovedBy? = null,
        @SerializedName("description")
        var description: String? = null,
        @SerializedName("totalAttempts")
        var totalAttempts: Int? = null,
        @SerializedName("totalCompleted")
        var totalCompleted: Int? = null,
        @SerializedName("totalStars")
        var totalStars: Int? = null,
        @SerializedName("voteScore")
        var voteScore: Int? = null,
        @SerializedName("tags")
        var tags: List<String>? = null,
        @SerializedName("contributorsWanted")
        var contributorsWanted: Boolean? = null,
        @SerializedName("unresolved")
        var unresolved: Unresolved? = null
)


fun ChallengeDetails.toChallengeDetailsData(): ChallengeDetailsData {
    return ChallengeDetailsData(
            name.orEmpty(),
            category.orEmpty(),
            createdBy?.username.orEmpty(),
            description.orEmpty(),
            rank?.name.orEmpty(),
            totalAttempts ?: -1,
            totalCompleted ?: -1,
            totalStars ?: -1,
            voteScore ?: -1
    )
}