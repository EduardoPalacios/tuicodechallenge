package com.epf.data.network.models.search

import com.google.gson.annotations.SerializedName

data class Overall(
        @SerializedName("rank")
        var rank: Int? = null,
        @SerializedName("name")
        var userName: String? = null,
        @SerializedName("color")
        var color: String? = null,
        @SerializedName("score")
        var score: Int? = null

)