package com.epf.data.network.models.challenges

import com.epf.data.room.entities.ChallengesCompletedEntity
import com.epf.data.room.entities.CompletedEntity
import com.google.gson.annotations.SerializedName


data class ChallengesCompleted(
        @SerializedName("totalPages")
        var totalPages: Int? = null,

        @SerializedName("totalItems")
        var totalItems: Int? = null,

        @SerializedName("data")
        var data: List<Completed>? = null
)

data class Completed(
        @SerializedName("id")
        var id: String? = null,

        @SerializedName("name")
        var name: String? = null,

        @SerializedName("slug")
        var slug: String? = null,

        @SerializedName("completedAt")
        var completedAt: String? = null,

        @SerializedName("completedLanguages")
        var completedLanguages: List<String>? = null,

        var pageNumber: Int = 0
) {

}

fun ChallengesCompleted.toEntity(id: Int = -1): ChallengesCompletedEntity {
    return ChallengesCompletedEntity(
            id,
            totalPages ?: -1,
            totalItems ?: -1
    )
}

fun Completed.toEntity(userId: Int = -1): CompletedEntity {
    return CompletedEntity(
            userId,
            id.orEmpty(),
            name.orEmpty(),
            slug.orEmpty(),
            completedAt.orEmpty(),
            pageNumber,
            completedLanguages.orEmpty()
    )
}