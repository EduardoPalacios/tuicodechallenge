package com.epf.data.network.models.challenges

import com.epf.data.room.entities.ChallengesAuthoredEntity
import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose


class ChallengesAuthored(

        @SerializedName("data")
        var challenges : List<Authored>? = null

)

class Authored(
        @SerializedName("id")
        var id: String? = null,

        @SerializedName("name")
        var name: String? = null,

        @SerializedName("description")
        var description: String? = null,

        @SerializedName("rank")
        var rank: Int? = null,

        @SerializedName("rankName")
        var rankName: String? = null,

        @SerializedName("tags")
        var tags: List<String>? = null,

        @SerializedName("languages")
        var languages: List<String>? = null
)


fun Authored.toAuthoredEntity(userId: Int): ChallengesAuthoredEntity {
        return ChallengesAuthoredEntity(
                userId,
                id.orEmpty(),
                name.orEmpty(),
                description.orEmpty(),
                rank ?: -1,
                rankName.orEmpty(),
                tags.orEmpty(),
                languages.orEmpty()
        )
}

