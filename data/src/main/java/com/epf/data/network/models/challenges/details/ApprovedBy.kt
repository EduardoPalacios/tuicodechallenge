package com.epf.data.network.models.challenges.details

import com.google.gson.annotations.SerializedName

class ApprovedBy(

        @SerializedName("username")
        var username: String? = null,
        @SerializedName("url")
        var url: String? = null

)