package com.epf.data.network.models.challenges.details

import com.google.gson.annotations.SerializedName

class Rank(
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("color")
        var color: String? = null

)