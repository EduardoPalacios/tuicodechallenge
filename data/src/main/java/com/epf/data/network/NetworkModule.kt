package com.epf.data.network

import com.epf.data.BuildConfig
import com.epf.data.BuildConfig.BASE_URL
import com.epf.domain.error.ErrorSource
import com.epf.domain.error.ErrorStream
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideApiService(okHttpClient: OkHttpClient): ApiService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()
            .create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideOkHttpClient(
            interceptor: () -> HttpLoggingInterceptor, @Named("AuthInterceptor") authInterceptor: Interceptor
    ): OkHttpClient =
            OkHttpClient.Builder().apply {
                connectTimeout(30, TimeUnit.SECONDS)
                readTimeout(30, TimeUnit.SECONDS)
                writeTimeout(30, TimeUnit.SECONDS)
                addInterceptor(interceptor())
                addInterceptor(authInterceptor)
            }.build()

    @Provides
    @Named("AuthInterceptor")
    fun providesAuthInterceptor(): Interceptor =
            Interceptor { chain ->
                val original = chain.request()
                val builder = original.newBuilder().method(original.method(), original.body())
                builder.header("Authorization", BuildConfig.API_KEY)
                chain.proceed(builder.build())
            }

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): () -> HttpLoggingInterceptor = { HttpLoggingInterceptor().setLevel((HttpLoggingInterceptor.Level.BODY)) }

    @Provides
    @Singleton
    fun provideErrorStream(errorSource: ErrorSource): ErrorStream = errorSource
}