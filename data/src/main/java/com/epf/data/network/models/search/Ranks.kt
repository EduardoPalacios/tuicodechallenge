package com.epf.data.network.models.search

import android.arch.persistence.room.Embedded
import com.google.gson.annotations.SerializedName

data class Ranks(

        @Embedded
        @SerializedName("overall")
        var overall: Overall? = null,

        @Embedded
        @SerializedName("languages")
        var userLanguages: Languages? = null

)
