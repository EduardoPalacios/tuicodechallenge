package com.epf.data.network.models.search

import com.epf.data.room.entities.UserEntity
import com.epf.domain.search.SearchData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class User(
        @SerializedName("username")
        val username: String? = null,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("honor")
        val honor: Int? = null,
        @SerializedName("clan")
        val clan: String? = null,
        @SerializedName("leaderboardPosition")
        val leaderboardPosition: Int? = null,
        @SerializedName("skills")
        val skills: List<String>? = null,
        @SerializedName("ranks")
        @Expose
        val ranks: Ranks? = null,
        @SerializedName("codeChallenges")
        @Expose
        val codeChallenges: CodeChallenges? = null
)


fun User.toSearchData(userId: Int): SearchData = SearchData(
        username.orEmpty(),
        ranks?.overall?.rank ?: -1,
        ranks?.findBestLanguage().orEmpty(),
        ranks?.overall?.score ?: -1,
        userId

)

fun Ranks.findBestLanguage() : String = userLanguages?.languages?.sortedByDescending { it.second.score }?.first()?.first.orEmpty()

fun User.toUserEntity(): UserEntity = UserEntity(
        username.orEmpty(),
        name.orEmpty(),
        honor ?: -1,
        clan.orEmpty(),
        leaderboardPosition ?: -1,
        skills.orEmpty(),
        ranks ?: Ranks(),
        codeChallenges ?: CodeChallenges()
)
