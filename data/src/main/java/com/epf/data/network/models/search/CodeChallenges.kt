package com.epf.data.network.models.search

import com.google.gson.annotations.SerializedName

data class CodeChallenges(
        @SerializedName("totalAuthored")
        var totalAuthored: Int? = null,
        @SerializedName("totalCompleted")
        var totalCompleted: Int? = null

)