package com.epf.data.room.converters

import android.arch.persistence.room.TypeConverter
import com.epf.data.network.models.challenges.Completed
import com.epf.data.network.models.search.Language
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken


class StringListConverter {

    private val gson = Gson()

    @TypeConverter
    fun to(list: List<String>): String = gson.toJson(list)

    @TypeConverter
    fun from(json: String): List<String> {
        val type = object : TypeToken<List<String>>() {}.type
        return gson.fromJson(json, type)
    }
}

class LanguagesConverter {

    private val gson = Gson()

    @TypeConverter
    fun to(list: List<Pair<String, Language>>): String = gson.toJson(list)

    @TypeConverter
    fun from(json: String): List<Pair<String, Language>> {
        val jsonObject = JsonParser().parse(json).asJsonArray
        return jsonObject.map {
            val language = it.asJsonObject.get("first").asString
            val data = Gson().fromJson(it.asJsonObject.get("second"), Language::class.java)
            language to data
        }

    }
}

class CompletedConverter{
    private val gson = Gson()

    @TypeConverter
    fun to(list: List<Completed>): String = gson.toJson(list)

    @TypeConverter
    fun from(json: String): List<Completed> {
        val type = object : TypeToken<List<Completed>>() {}.type
        return gson.fromJson<List<Completed>>(json, type)
//        val jsonObject = JsonParser().parse(json).asJsonArray
//        return jsonObject.map {
//            val language = it.asJsonObject.get("first").asString
//            val data = Gson().fromJson(it.asJsonObject.get("second"), Language::class.java)
//            language to data
//        }

    }
}
