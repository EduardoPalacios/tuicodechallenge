package com.epf.data.room

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.arch.persistence.room.*
import com.epf.data.room.TuiDatabase.Companion.DATABASE_VERSION
import com.epf.data.room.entities.*
import io.reactivex.Maybe

@Database(entities = [
    UserEntity::class,
    ChallengesCompletedEntity::class,
    CompletedEntity::class,
    PageEntity::class,
    ChallengesAuthoredEntity::class
], version = DATABASE_VERSION, exportSchema = false)
abstract class TuiDatabase : RoomDatabase() {

    companion object {

        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "tui_room_db"
    }

    abstract fun userDao(): UserDao
    abstract fun challengesDao(): ChallengesDao
    abstract fun pageDao(): PageDao
}


@Dao
interface UserDao {

    companion object {
        const val SELECT_ALL_USERS = "SELECT * FROM ${UserEntity.USER_ENTITY}"
        const val SELECT_USER_NAME = "SELECT user_name FROM ${UserEntity.USER_ENTITY}"
    }

    @Query(value = "$SELECT_ALL_USERS ORDER BY _id DESC LIMIT 5")
    fun getRecentUsers(): Maybe<List<UserEntity>>

    @Query(value = "$SELECT_ALL_USERS ORDER BY _id DESC LIMIT 1")
    fun getLastUser(): UserEntity

    @Query(value = "$SELECT_USER_NAME WHERE user_name = :idOrName OR name = :idOrName")
    fun findUserBy(idOrName: String): String

    @Query(value = SELECT_ALL_USERS)
    fun getEntities(): List<UserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addMember(userEntity: UserEntity)

}

@Dao
interface ChallengesDao {

    companion object {
        const val SELECT_CHALLENGES = "SELECT * FROM ${ChallengesCompletedEntity.CHALLENGES_COMPLETED_ENTITY}"
        const val SELECT_COMPLETED = "SELECT * FROM ${CompletedEntity.COMPLETED_ENTITY}"
        const val SELECT_AUTHORED = "SELECT * FROM ${ChallengesAuthoredEntity.CHALLENGES_AUTHORED_ENTITY}"
    }


    @Query(value = "$SELECT_COMPLETED WHERE userId = :memberId")
    fun getChallengesCompleted(memberId: Int): DataSource.Factory<Int, CompletedEntity>

    @Query(value = SELECT_CHALLENGES)
    fun getAllChallengesCompleted(): List<ChallengesCompletedEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addChallengesCompleted(userEntity: ChallengesCompletedEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCompleted(userEntity: CompletedEntity)

    @Query(value = "$SELECT_AUTHORED WHERE userId = :memberId")
    fun getAllAuthored(memberId: Int): LiveData<List<ChallengesAuthoredEntity>>

    @Query(value = SELECT_AUTHORED)
    fun getAllAuthoredList(): List<ChallengesAuthoredEntity>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAuthored(authoredEntity: List<ChallengesAuthoredEntity>)

}

@Dao
interface PageDao {
    companion object {
        const val SELECT_ALL = "SELECT * FROM ${PageEntity.PAGE_ENTITY}"
        const val SELECT_PAGE = "SELECT page FROM ${PageEntity.PAGE_ENTITY}"
    }

    @Query(value = "$SELECT_ALL WHERE userId = :memberId AND page <= totalPages ORDER BY ${PageEntity.COLUMN_ID} DESC LIMIT 1")
    fun getPage(memberId: Int): PageEntity?

    @Query(SELECT_ALL)
    fun getAll(): List<PageEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(pageEntity: PageEntity)

}