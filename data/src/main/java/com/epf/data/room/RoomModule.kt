package com.epf.data.room

import android.arch.persistence.room.Room
import android.content.Context
import com.epf.data.room.TuiDatabase.Companion.DATABASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {

    @Provides
    @Singleton
    fun provideTuiRoom(context: Context): TuiDatabase = Room.databaseBuilder(context, TuiDatabase::class.java, DATABASE_NAME)
            .allowMainThreadQueries()
            .build()

    @Provides
    @Singleton
    fun provideMemberDao(database: TuiDatabase): UserDao = database.userDao()

    @Provides
    @Singleton
    fun provideChallengesDao(database: TuiDatabase): ChallengesDao = database.challengesDao()


    @Provides
    @Singleton
    fun providePageDao(database: TuiDatabase): PageDao = database.pageDao()
}