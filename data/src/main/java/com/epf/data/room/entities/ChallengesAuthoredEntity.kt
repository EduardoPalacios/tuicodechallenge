package com.epf.data.room.entities

import android.arch.persistence.room.*
import com.epf.data.room.converters.StringListConverter
import com.epf.data.room.entities.ChallengesAuthoredEntity.Companion.AUTHORED_ID
import com.epf.data.room.entities.ChallengesAuthoredEntity.Companion.CHALLENGES_AUTHORED_ENTITY
import com.epf.data.room.entities.ChallengesCompletedEntity.Companion.USER_ID

@Entity(tableName = CHALLENGES_AUTHORED_ENTITY, indices = [(Index(value = [AUTHORED_ID], unique = true))])
@TypeConverters(StringListConverter::class)
class ChallengesAuthoredEntity constructor(

        @ColumnInfo(name = USER_ID)
        var userId: Int = -1, // points to UserEntity

        @ColumnInfo(name = AUTHORED_ID)
        var authoredId: String = "",

        @ColumnInfo(name = NAME)
        var name: String = "",

        @ColumnInfo(name = DESCRIPTION)
        var description: String = "",

        @ColumnInfo(name = RANK)
        var rank: Int = -1,

        @ColumnInfo(name = RANk_NAME)
        var rankName: String = "",

        @ColumnInfo(name = TAGS)
        var tags: List<String> = emptyList(),

        @ColumnInfo(name = LANGUAGES)
        var languages: List<String> = emptyList()


) {

    @ColumnInfo(name = COLUMN_ID)
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    companion object {
        const val COLUMN_ID = "_id"
        const val CHALLENGES_AUTHORED_ENTITY = "ChallengesAuthoredEntity"
        const val AUTHORED_ID = "id"
        const val RANK = "rank"
        const val NAME = "name"
        const val DESCRIPTION = "description"
        const val RANk_NAME = "rankName"
        const val TAGS = "tags"
        const val LANGUAGES = "languages"


    }


}