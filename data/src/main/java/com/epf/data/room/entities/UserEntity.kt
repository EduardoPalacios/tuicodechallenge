package com.epf.data.room.entities

import android.arch.persistence.room.*
import com.epf.data.network.models.search.CodeChallenges
import com.epf.data.network.models.search.Ranks
import com.epf.data.network.models.search.findBestLanguage
import com.epf.data.room.converters.StringListConverter
import com.epf.data.room.entities.UserEntity.Companion.USER_ENTITY
import com.epf.domain.search.SearchData

@Entity(tableName = USER_ENTITY, indices = [Index(value = [UserEntity.USER_NAME], unique = true)])
@TypeConverters(StringListConverter::class)
data class UserEntity constructor(

        @ColumnInfo(name = USER_NAME)
        var username: String = "",

        @ColumnInfo(name = NAME)
        var name: String = "",

        @ColumnInfo(name = HONOR)
        var honor: Int = -1,

        @ColumnInfo(name = CLAN)
        var clan: String = "",

        @ColumnInfo(name = LEADER_BOARD_POSITION)
        var leaderboardPosition: Int = -1,

        @ColumnInfo(name = SKILLS)
        var skills: List<String> = emptyList(),

        @Embedded
        var ranks: Ranks = Ranks(),

        @Embedded
        var codeChallenges: CodeChallenges = CodeChallenges()
) {

    companion object {
        const val USER_ENTITY = "userEntity"
        const val COLUMN_ID = "_id"
        const val USER_NAME = "user_name"
        const val NAME = "name"
        const val HONOR = "honor"
        const val CLAN = "clan"
        const val LEADER_BOARD_POSITION = "leader_board_position"
        const val SKILLS = "skils"
    }

    @ColumnInfo(name = COLUMN_ID)
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

}

fun UserEntity.toSearchData() = SearchData(
        username,
        ranks.overall?.rank ?: -1,
        ranks.findBestLanguage(),
        ranks.overall?.score ?: -1,
        id
)