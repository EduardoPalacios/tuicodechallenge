package com.epf.data.room.entities

import android.arch.persistence.room.*
import com.epf.data.room.converters.StringListConverter
import com.epf.data.room.entities.ChallengesCompletedEntity.Companion.CHALLENGES_COMPLETED_ENTITY
import com.epf.data.room.entities.ChallengesCompletedEntity.Companion.USER_ID
import com.epf.data.room.entities.CompletedEntity.Companion.COMPLETED_ENTITY
import com.epf.data.room.entities.CompletedEntity.Companion.COMPLETED_ID
import com.epf.data.room.entities.PageEntity.Companion.PAGE_ENTITY

@Entity(tableName = CHALLENGES_COMPLETED_ENTITY, indices = [(Index(value = [USER_ID], unique = true))])
data class ChallengesCompletedEntity constructor(

        @ColumnInfo(name = USER_ID)
        var userId: Int = -1, // points to UserEntity

        @ColumnInfo(name = TOTAL_PAGES)
        var totalPages: Int = -1,

        @ColumnInfo(name = TOTAL_ITEMS)
        var totalItems: Int = -1

) {

    @ColumnInfo(name = COLUMN_ID)
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0


    companion object {
        const val CHALLENGES_COMPLETED_ENTITY = "challengesCompletedEntity"
        const val COLUMN_ID = "_id"
        const val USER_ID = "userId"
        const val TOTAL_PAGES = "totalPages"
        const val TOTAL_ITEMS = "totalItems"
        const val COMPLETED = "completed"
    }

}


@Entity(tableName = COMPLETED_ENTITY, indices = [(Index(value = [COMPLETED_ID], unique = true))])
@TypeConverters(StringListConverter::class)
data class CompletedEntity(

        @ColumnInfo(name = USER_ID)
        var userId: Int = -1,

        @ColumnInfo(name = COMPLETED_ID)
        var completed_id: String = "",

        @ColumnInfo(name = NAME)
        var name: String = "",

        @ColumnInfo(name = SLUG)
        var slug: String = "",

        @ColumnInfo(name = COMPLETED_AT)
        var completedAt: String = "",

        @ColumnInfo(name = PAGE_NUMBER)
        var pageNumber: Int = -1,


        var completedLanguages: List<String> = emptyList()
) {

    @ColumnInfo(name = CompletedEntity.COLUMN_ID)
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    companion object {
        const val COMPLETED_ENTITY = "completedEntity"
        const val COMPLETED_ID = "id"
        const val COLUMN_ID = "_id"
        const val NAME = "name"
        const val SLUG = "slug"
        const val PAGE_NUMBER = "pageNumber"
        const val COMPLETED_AT = "completedAt"
        const val COMPLETED_LANGUAGES = "completedLanguages"
    }
}


@Entity(tableName = PAGE_ENTITY, indices = [(Index(value = [(PageEntity.USER_NAME)], unique = true))])
data class PageEntity(

        @ColumnInfo(name = USER_ID)
        var userId: Int = -1, // points to ChallengesCompletedEntity

        @ColumnInfo(name = USER_NAME)
        var userName: String = "",

        @ColumnInfo(name = PAGE)
        var page: Int = -1,

        @ColumnInfo(name = TOTAL_PAGES)
        var total: Int = -1
) {

    companion object {
        const val PAGE_ENTITY = "pageEntity"
        const val USER_NAME = "userName"
        const val PAGE = "page"
        const val TOTAL_PAGES = "totalPages"
        const val COLUMN_ID = "_id"
    }

    @ColumnInfo(name = PageEntity.COLUMN_ID)
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

}
