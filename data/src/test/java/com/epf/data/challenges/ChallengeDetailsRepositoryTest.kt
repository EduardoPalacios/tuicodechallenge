package com.epf.data.challenges

import com.epf.data.network.ApiService
import com.epf.data.network.models.challenges.details.ChallengeDetails
import com.epf.domain.challenges.ChallengeDetailsRepository
import com.epf.domain.error.ErrorStream
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class ChallengeDetailsRepositoryTest {

    @Mock
    lateinit var mockApiService: ApiService

    @Mock
    lateinit var mockErrorStream: ErrorStream

    lateinit var sut: ChallengeDetailsRepository

    @Before
    fun setUp() {
        sut = ChallengeDetailsRepositoryImpl(mockApiService, mockErrorStream)
    }

    @Test
    fun givenResponseThenReturnChallengeDetailsData() {
        `when`(mockApiService.getChallengeDetails("id")).thenReturn(Single.just(Response.success(ChallengeDetails())))
        sut.requestChallengeDetails("id")
    }
}