package com.epf.data.challenges

import com.epf.data.network.ApiService
import com.epf.data.network.models.challenges.ChallengesCompleted
import com.epf.data.network.models.challenges.Completed
import com.epf.data.room.ChallengesDao
import com.epf.data.room.PageDao
import com.epf.data.room.entities.ChallengesCompletedEntity
import com.epf.data.room.entities.CompletedEntity
import com.epf.data.room.entities.PageEntity
import com.epf.domain.challenges.ChallengesRepository
import com.epf.domain.error.ErrorStream
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers

import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class ChallengesRepositoryImplTest {

    @Mock
    lateinit var mockService: ApiService
    @Mock
    lateinit var mockChallengesDao: ChallengesDao
    @Mock
    lateinit var mockPageDao: PageDao
    @Mock
    lateinit var mockErrorStream: ErrorStream

    private lateinit var sut: ChallengesRepository

    private val memberId = 3
    private val idOrName = "idOrName"

    @Before
    fun setUp() {
        sut = ChallengesRepositoryImpl(mockService, mockChallengesDao, mockPageDao, mockErrorStream)
    }

    @Test
    fun givenNoPageThenRequestFirstPage() {
        `when`(mockPageDao.getPage(memberId)).thenReturn(null)
        val challengesCompleted = ChallengesCompleted()
        `when`(mockService.getChallengesCompleted(idOrName, 0)).thenReturn(Single.just(Response.success(challengesCompleted)))
        sut.getChallengesCompleted(idOrName, memberId).test()
        verify(mockService).getChallengesCompleted(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())
    }

    @Test
    fun givenPageThenRequestNextPage() {
        `when`(mockPageDao.getPage(memberId)).thenReturn(PageEntity(page = 2, total = 5))
        val challengesCompleted = ChallengesCompleted(totalPages = 5, totalItems = 3)
        `when`(mockService.getChallengesCompleted(idOrName, 3)).thenReturn(Single.just(Response.success(challengesCompleted)))
        sut.getChallengesCompleted(idOrName, memberId).test()
        verify(mockService).getChallengesCompleted(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())
    }

    @Test
    fun givenInvalidCompletedChallengesTheDontCacheThem() {
        `when`(mockPageDao.getPage(memberId)).thenReturn(null)
        `when`(mockService.getChallengesCompleted(idOrName, 0)).thenReturn(Single.just(Response.success(ChallengesCompleted())))
        sut.getChallengesCompleted(idOrName, memberId).test()
        verify(mockChallengesDao, never()).addChallengesCompleted(ChallengesCompletedEntity())
    }

    @Test
    fun givenChallengesThenCacheData() {
        `when`(mockPageDao.getPage(memberId)).thenReturn(null)
        `when`(mockService.getChallengesCompleted(idOrName, 0)).thenReturn(Single.just(Response.success(ChallengesCompleted(data = listOf(Completed())))))
        sut.getChallengesCompleted(idOrName, memberId).test()
        verify(mockChallengesDao).addChallengesCompleted(ChallengesCompletedEntity(userId = memberId))
        verify(mockChallengesDao).addCompleted(CompletedEntity(userId = memberId, pageNumber = 0))
        verify(mockPageDao).add(PageEntity(3, idOrName, 0, 0))
    }

    @Test
    fun givenError() {

    }
}