package com.epf.data.network.models

import com.epf.data.network.models.search.*
import com.epf.domain.search.SearchData
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class UserMapperTest {

    private lateinit var sut: User
    private val userId = 1

    @Before
    fun setUp() {
        sut = User(username = "eddyVolcano", ranks = Ranks(Overall(rank = 1, score = 10), userLanguages = Languages(listOf("java" to Language(score = 2), "kotlin" to Language(score = 7)))))
    }

    @Test
    fun givenUserThenMapToSearchData() {
        val expected = SearchData("eddyVolcano", 1, "kotlin", 10, 1)
        assertEquals(expected, sut.toSearchData(userId))
    }

    @Test
    fun givenNullableValueThenFallbackToDefaultValues() {
        sut = User(username = "eddyVolcano", ranks = Ranks(Overall(rank = 1, score = 10), userLanguages = null))
        val expected = SearchData("eddyVolcano", 1, "", 10, 1)
        assertEquals(expected, sut.toSearchData(userId))
    }


}