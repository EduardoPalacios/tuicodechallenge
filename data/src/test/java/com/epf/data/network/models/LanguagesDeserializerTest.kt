package com.epf.data.network.models

import com.epf.data.network.models.search.Language
import com.epf.data.network.models.search.Languages
import com.epf.data.network.models.search.LanguagesDeserializer
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class LanguagesDeserializerTest {

    lateinit var sut: LanguagesDeserializer

    private val json = "{ \"javascript\": { \"rank\": -3, \"name\": \"3 kyu\", \"color\": \"blue\", \"score\": 1819 }, \"ruby\": { \"rank\": -4, \"name\": \"4 kyu\", \"color\": \"blue\", \"score\": 1005 } }"
    private var typeToken = object : TypeToken<Languages>() {}.type

    @Before
    fun setUp() {
        sut = LanguagesDeserializer()
    }

    @Test
    fun givenResponseThenDeserializeLanguage() {
        val javascript = "javascript" to Language(-3, "3 kyu", "blue", 1819)
        val ruby = "ruby" to Language(-4, "4 kyu", "blue", 1005)
        val expected = Languages(listOf(javascript, ruby))

        val toJson = JsonParser().parse(json).asJsonObject
        val actual = sut.deserialize(toJson, typeToken, null)

        assertEquals(expected, actual)
    }
}