package com.epf.data.search

import com.epf.data.network.ApiService
import com.epf.data.network.models.search.*
import com.epf.data.room.UserDao
import com.epf.data.room.entities.UserEntity
import com.epf.domain.error.ErrorStream
import com.epf.domain.search.SearchData
import com.epf.domain.search.SearchRepository
import io.reactivex.Maybe
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class SearchRepositoryImplTest {

    @Mock
    lateinit var mockApiService: ApiService
    @Mock
    lateinit var mockDao: UserDao
    @Mock
    lateinit var mockErrorStream: ErrorStream

    private lateinit var sut: SearchRepository<SearchData>

    @Before
    fun setUp() {
        sut = SearchRepositoryImpl(mockApiService, mockDao, mockErrorStream)
    }

    @Test
    fun givenAlreadyExistingUserThenIgnoreSearchResult() {
        val name = "eddy"
        `when`(mockDao.findUserBy(name)).thenReturn(name)
        sut.getUser(name).test().assertValue(SearchData())
    }

    @Test
    fun givenUserIdOrNameThenSaveSearchData() {
        val expected = UserEntity()
        val id = "eddyVolcano"
        `when`(mockApiService.getUser(id)).thenReturn(Single.just(Response.success(User())))
        sut.getUser(id).test()
        verify(mockDao).addMember(expected)
    }

    @Test
    fun givenRequestForCachedDataThenReturnLastFiveMembers() {
        val member1 = SearchData("name1", 1, "java", 10, 1)
        val member2 = SearchData("name2", -7, "java", 10, 2)
        val member3 = SearchData("name3", 3, "java", 10, 3)
        val member4 = SearchData("name4", 5, "java", 10, 4)
        val member5 = SearchData("name5", -5, "java", 10, 5)
        val recentMembers = listOf<SearchData>(member1, member2, member3, member4, member5)

        val entity1 = UserEntity(username = "name1", ranks = Ranks(Overall(rank = 1, score = 10), userLanguages = Languages(listOf("java" to Language())))).apply { id = 1 }
        val entity2 = UserEntity(username = "name2", ranks = Ranks(Overall(rank = -7, score = 10), userLanguages = Languages(listOf("java" to Language())))).apply { id = 2 }
        val entity3 = UserEntity(username = "name3", ranks = Ranks(Overall(rank = 3, score = 10), userLanguages = Languages(listOf("java" to Language())))).apply { id = 3 }
        val entity4 = UserEntity(username = "name4", ranks = Ranks(Overall(rank = 5, score = 10), userLanguages = Languages(listOf("java" to Language())))).apply { id = 4 }
        val entity5 = UserEntity(username = "name5", ranks = Ranks(Overall(rank = -5, score = 10), userLanguages = Languages(listOf("java" to Language())))).apply { id = 5 }
        val recentEntities = listOf<UserEntity>(entity1, entity2, entity3, entity4, entity5)


        `when`(mockDao.getRecentUsers()).thenReturn(Maybe.just(recentEntities))
        sut.getCachedData().test().assertValue(recentMembers)
    }


}