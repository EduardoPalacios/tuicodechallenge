package com.epf.domain.challenges

data class ChallengeDetailsData(
        val name: String = "",
        val category: String = "",
        val createdBy: String = "",
        val description: String = "",
        val rank: String = "",
        val totalAttempts: Int = -1,
        val totalCompleted: Int = -1,
        val totalStars: Int = -1,
        val voteScore: Int = -1
)
