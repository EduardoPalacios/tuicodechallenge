package com.epf.domain.challenges

import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject


interface ChallengesUseCase {
    fun requestCompleted(idOrName: String, memberId: Int): Completable
    fun requestAuthored(idOrName: String, memberId: Int): Completable
}

class ChallengesUseCaseImpl @Inject constructor(private val repository: ChallengesRepository) : ChallengesUseCase {

    override fun requestCompleted(idOrName: String, memberId: Int): Completable {
        return repository.getChallengesCompleted(idOrName, memberId)
    }

    override fun requestAuthored(idOrName: String, memberId: Int): Completable {
        return repository.getChallengesAuthored(idOrName, memberId)
    }


}


interface ChallengeDetailsUseCase {
    fun requestChallengeDetails(challengeId: String): Single<ChallengeDetailsData>
}

class ChallengeDetailsUseCaseImpl @Inject constructor(val repository: ChallengeDetailsRepository) : ChallengeDetailsUseCase {

    override fun requestChallengeDetails(challengeId: String): Single<ChallengeDetailsData> {
        return repository.requestChallengeDetails(challengeId)
    }
}