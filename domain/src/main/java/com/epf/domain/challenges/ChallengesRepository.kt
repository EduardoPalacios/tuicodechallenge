package com.epf.domain.challenges

import io.reactivex.Completable
import io.reactivex.Single


interface ChallengesRepository {

    fun getChallengesCompleted(idOrName: String, memberId: Int): Completable
    fun getChallengesAuthored(idOrName: String, memberId: Int): Completable
}

interface ChallengeDetailsRepository {
    fun requestChallengeDetails(challengeId: String): Single<ChallengeDetailsData>
}