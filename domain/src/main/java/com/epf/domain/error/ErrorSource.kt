package com.epf.domain.error

import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject
import java.net.UnknownHostException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ErrorSource @Inject constructor() : ErrorStream {

    private val errorPublish = PublishSubject.create<ErrorType>()

    fun onErrorFlowable(): Flowable<ErrorType> = errorPublish.toFlowable(BackpressureStrategy.DROP)

    override fun processErrorCode(code: Int) {
        when (code) {
            404 -> {
                errorPublish.onNext(ErrorType.NotFound)
            }
        }
    }

    override fun processException(throwable: Throwable) {
        when (throwable) {
            is UnknownHostException -> {
                errorPublish.onNext(ErrorType.NoConnectivity)
            }
            else -> {
                errorPublish.onNext(ErrorType.Unknown)
            }
        }

    }
}


