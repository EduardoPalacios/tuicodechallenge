package com.epf.domain.error

interface ErrorStream{
    fun processErrorCode(code: Int)
    fun processException(throwable: Throwable)

}


sealed class ErrorType{
    object NotFound: ErrorType()
    object NoConnectivity: ErrorType()
    object Unknown : ErrorType()
}