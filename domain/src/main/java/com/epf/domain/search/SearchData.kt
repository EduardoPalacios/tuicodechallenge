package com.epf.domain.search

data class SearchData(
        val name: String = "",
        val rank: Int = -1,
        val bestLanguage: String = "",
        val score: Int = -1,
        val id : Int = -1
)

fun SearchData.isValid(): Boolean = name.isNotBlank()