package com.epf.domain.search

import io.reactivex.Maybe
import io.reactivex.Single

interface SearchRepository<T> {

    fun getUser(idOrName: String): Single<T>
    fun getCachedData(): Maybe<List<T>>
}