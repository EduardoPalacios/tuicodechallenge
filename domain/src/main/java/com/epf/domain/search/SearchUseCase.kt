package com.epf.domain.search

import io.reactivex.Maybe
import io.reactivex.Single
import java.lang.IllegalArgumentException
import javax.inject.Inject

interface UseCase {
    fun request(idOrName: String): Single<SearchData>
    fun requestCache(): Maybe<List<SearchData>>
}

class SearchUseCase @Inject constructor(private val repository: SearchRepository<SearchData>) : UseCase {


    override fun request(idOrName: String): Single<SearchData> {
        if(idOrName.isBlank()){return Single.error(IllegalArgumentException("ignored empty search"))}
        return repository.getUser(idOrName)
    }

    override fun requestCache(): Maybe<List<SearchData>> {
        return repository.getCachedData()
    }
}