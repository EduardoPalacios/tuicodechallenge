package com.epf.domain.search

import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class SearchUseCaseTest {

    @Mock
    private lateinit var mockRepository: SearchRepository<SearchData>

    private lateinit var sut: SearchUseCase

    @Before
    fun setUp() {
        sut = SearchUseCase(mockRepository)
    }

    @Test
    fun givenValidInputThenGetUser() {
        val expected = SearchData("name")
        val idOrName = "eddyVolcano"
        `when`(mockRepository.getUser(idOrName)).thenReturn(Single.just(expected))
        sut.request(idOrName).test()
                .assertNoErrors()
                .assertValue(expected)
                .assertComplete()
    }

    @Test
    @Throws(IllegalArgumentException::class)
    fun givenInvalidInputThenGetEmptyUser() {
        val idOrName = ""
        sut.request(idOrName).test()
                .assertError(IllegalArgumentException::class.java)
    }

    @Test
    fun givenRequestToLoadCacheThenGetDataFromRepository() {

        sut.requestCache()
        verify(mockRepository).getCachedData()
    }
}